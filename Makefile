#!make

BUILD_CACHE_PREFIX="ideaplexus/"
PROXY_CACHE_PREFIX=""

.PHONY: lint
lint: lint-1.24 lint-1.23 lint-1.22 lint-1.21 lint-1.20 lint-1.19 lint-1.18 lint-1.17

lint-1.24:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine< 1.24/Dockerfile

lint-1.23:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine< 1.23/Dockerfile

lint-1.22:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine< 1.22/Dockerfile

lint-1.21:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine< 1.21/Dockerfile

lint-1.20:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine< 1.20/Dockerfile

lint-1.19:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine< 1.19/Dockerfile

lint-1.18:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine< 1.18/Dockerfile

lint-1.17:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine< 1.17/Dockerfile

.PHONY: build
build: build-1.24 build-1.23 build-1.22 build-1.21 build-1.20 build-1.19 build-1.18 build-1.17

build-1.24:
	docker build --pull --file 1.24/Dockerfile --tag "$(BUILD_CACHE_PREFIX)go:1.24" --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" .

build-1.23:
	docker build --pull --file 1.23/Dockerfile --tag "$(BUILD_CACHE_PREFIX)go:1.23" --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" .

build-1.22:
	docker build --pull --file 1.22/Dockerfile --tag "$(BUILD_CACHE_PREFIX)go:1.22" --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" .

build-1.21:
	docker build --pull --file 1.21/Dockerfile --tag "$(BUILD_CACHE_PREFIX)go:1.21" --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" .

build-1.20:
	docker build --pull --file 1.20/Dockerfile --tag "$(BUILD_CACHE_PREFIX)go:1.20" --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" .

build-1.19:
	docker build --pull --file 1.19/Dockerfile --tag "$(BUILD_CACHE_PREFIX)go:1.19" --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" .

build-1.18:
	docker build --pull --file 1.18/Dockerfile --tag "$(BUILD_CACHE_PREFIX)go:1.18" --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" .

build-1.17:
	docker build --pull --file 1.17/Dockerfile --tag "$(BUILD_CACHE_PREFIX)go:1.17" --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" .

.PHONY: all
all: lint build